# Document Permission System

- All routes are tested with `pytest`
- To run test export `TESTING=True`
  ```bash
  $ poetry install && poetry shell
  $ export TESTING=True
  $ python -m pytest
  ```
- It generates sqlite in memory to test functions
- I covered all test cases that provided in README.md with some modification on values, but the structure of response is same
- You can export `DB_HOST`, `DB_PORT`, `DB_NAME`, `DB_USER`, `DB_PASSWORD` to test on your local machine

  - It assumes you are using `postgres` as database
