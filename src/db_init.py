from tortoise.contrib.fastapi import HTTPNotFoundError, register_tortoise
from settings import db_user, db_password, db_host, db_port, db_name
from models import Member, Category, Document


def init(app):
    if app.testing:
        db_url = "sqlite://:memory:"
    else:
        db_url = f"postgres://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}"
    register_tortoise(
        app,
        db_url=db_url,
        modules={"models": ["models"]},
        generate_schemas=True,
        add_exception_handlers=True,
    )
