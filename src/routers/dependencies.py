from fastapi import HTTPException, Header, Request
from models.member import Member


async def get_current_user(username: str = Header(None)):
    member = await Member.get_or_none(username=username)
    return member


async def is_login(username: str = Header(None)):
    if username is None:
        raise HTTPException(status_code=401, detail="No authorization header!")
    member = await Member.get_or_none(username=username)
    if member is None:
        raise HTTPException(status_code=401, detail="No authorization header!")


async def is_admin(username: str = Header(None)):
    if username == "admin":
        return True
    raise HTTPException(status_code=403, detail="Permission Denied!")


async def check_permission(request: Request, username: str = Header(None)):
    permission_dict = {
        "GET": "can_read",
        "POST": "can_create",
        "DELETE": "can_delete",
        "PUT": "can_update",
    }
    member = await Member.get_or_none(username=username)

    if category_id := request.query_params.get("category_id"):
        permission_name = permission_dict[request.method]
        has_permission = await member.has_category_permission(
            category_id=int(category_id), type=permission_name
        )

        if has_permission:
            return True

    if document_id := request.path_params.get("document_id"):
        permission_name = permission_dict[request.method]
        has_permission = await member.has_document_permission(
            document_id=int(document_id), type=permission_name
        )

        if has_permission:
            return True
    if not document_id and not category_id:
        return True
    raise HTTPException(status_code=403, detail="Permission Denied!")