from typing import List
from fastapi import APIRouter, Depends, HTTPException
from models import Category, Document
from pydantic import BaseModel
from .dependencies import get_current_user, check_permission, is_login


from tortoise.models import Q
from models.member import Member
from routers.member import MemberOut


router = APIRouter(dependencies=[Depends(is_login), Depends(check_permission)])


class CategoryIn(BaseModel):
    name: str


class CategoryOut(BaseModel):
    id: int
    name: str


class DocumentIn(BaseModel):
    subject: str
    content: str


class DocumentOut(BaseModel):
    id: int
    subject: str
    content: str
    author: MemberOut
    category: CategoryOut


class DocumentPermissionOut(BaseModel):
    id: int
    author: MemberOut
    category: CategoryOut
    can_create: bool
    can_read: bool
    can_update: bool
    can_delete: bool


@router.post("/create", response_model=DocumentOut)
async def create_document(
    document: DocumentIn,
    category_id: int = None,
    member: Member = Depends(get_current_user),
):
    category = await Category.get(id=category_id)

    document = await Document.create(
        subject=document.subject,
        content=document.content,
        author=member,
        category=category,
    )
    data = await document.to_dict()
    return DocumentOut(**data)


@router.get("/{document_id}", response_model=DocumentOut)
async def read_document(
    document_id: int,
):
    document = await Document.get(id=document_id)
    if document is None:
        raise HTTPException(status_code=404, detail="Document Not Found!")
    data = await document.to_dict()
    return DocumentOut(**data)


@router.put("/{document_id}", response_model=DocumentOut)
async def update_document(
    document_id: int,
    document: DocumentIn,
):
    document_instance: Document = await Document.filter(id=document_id).first()
    if document_instance is None:
        raise HTTPException(status_code=404, detail="Document Not Found!")
    document_instance.subject = document.subject
    document_instance.content = document.content
    await document_instance.save()
    data = await document_instance.to_dict()
    return DocumentOut(**data)


@router.delete("/{document_id}")
async def delete_document(
    document_id: int,
):
    document_instance: Document = await Document.filter(id=document_id).first()
    if document_instance is None:
        raise HTTPException(status_code=404, detail="Document Not Found!")
    await document_instance.delete()
    return None


@router.get("/", response_model=List[DocumentPermissionOut])
async def read_documents(
    member: Member = Depends(get_current_user),
):
    documents_ids = await member.documentpermissions.filter(can_read=True).values_list(
        "document_id", flat=True
    )
    category_ids = await member.categorypermissions.filter(can_read=True).values_list(
        "category_id", flat=True
    )

    documents = await Document.filter(
        Q(id__in=documents_ids) | Q(category_id__in=category_ids)
    ).all()

    return [await document.to_dict_with_permissions() for document in documents]