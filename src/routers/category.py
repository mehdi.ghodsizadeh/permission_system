from fastapi import APIRouter, Depends, Header, HTTPException, Request
from models import Category
from pydantic import BaseModel

from models.member import Member


router = APIRouter()


class CategoryIn(BaseModel):
    name: str


class CategoryOut(BaseModel):
    id: int
    name: str


@router.post("/create", response_model=CategoryOut)
async def create_category(category: CategoryIn, username: str = Header(None)):
    if username is None:
        raise HTTPException(status_code=401, detail="No authorization header!")

    member = await Member.filter(username=username).first()
    if member is None:
        raise HTTPException(status_code=401, detail="User Not Found!")

    if member.username != "admin":
        raise HTTPException(status_code=403, detail="Permission Denied!")

    category = await Category.create(name=category.name)
    return CategoryOut(id=category.id, name=category.name)
