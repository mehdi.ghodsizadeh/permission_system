from http.client import HTTPException
from tempfile import NamedTemporaryFile
from typing import Optional, Union
from fastapi import APIRouter, Depends
from pydantic import BaseModel
from models.permission import CategoryPermission, DocumentPermission

from routers.dependencies import is_admin, is_login

router = APIRouter(dependencies=[Depends(is_login), Depends(is_admin)])


class PermissionModel(BaseModel):
    category_id: Optional[int]
    document_id: Optional[int]
    member_id: int
    can_create: bool
    can_read: bool
    can_update: bool
    can_delete: bool


class CategoryPermissionOut(BaseModel):
    category_id: int
    member_id: int
    can_create: bool
    can_read: bool
    can_update: bool
    can_delete: bool


class DocumentPermissionOut(BaseModel):
    document_id: int
    member_id: int
    can_create: bool
    can_read: bool
    can_update: bool
    can_delete: bool


@router.post(
    "/create", response_model=Union[CategoryPermissionOut, DocumentPermissionOut]
)
async def create_permission(permission: PermissionModel):
    if permission.category_id:

        return await CategoryPermission.create(
            category_id=permission.category_id,
            member_id=permission.member_id,
            can_create=permission.can_create,
            can_read=permission.can_read,
            can_update=permission.can_update,
            can_delete=permission.can_delete,
        )

    elif permission.document_id:
        return await DocumentPermission.create(
            document_id=permission.document_id,
            member_id=permission.member_id,
            can_create=permission.can_create,
            can_read=permission.can_read,
            can_update=permission.can_update,
            can_delete=permission.can_delete,
        )
    raise HTTPException(status_code=400, detail="Invalid permission")
