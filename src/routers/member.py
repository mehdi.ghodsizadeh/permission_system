from fastapi import APIRouter
from models import Member
from pydantic import BaseModel

router = APIRouter()


class MemberIn(BaseModel):
    username: str


class MemberOut(BaseModel):
    id: int
    username: str


@router.post("/create", response_model=MemberOut)
async def create_member(
    member: MemberIn,
):
    member = Member(username=member.username)
    await member.save()
    return member
