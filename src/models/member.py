from tortoise import Model, fields
from tortoise.contrib.pydantic import pydantic_model_creator
from models.document import Document

from models.permission import CategoryPermission, DocumentPermission


class Member(Model):
    id = fields.IntField(pk=True)
    username = fields.CharField(max_length=128, unique=True)

    async def has_create_category_permission(self, type: str) -> bool:
        return self.username == "admin"

    async def has_category_permission(
        self,
        category_id: int,
        type: str,
    ) -> bool:

        category_permission = await self.categorypermissions.filter(
            category_id=category_id, member_id=self.id
        ).first()
        if category_permission is None:
            return False
        return getattr(category_permission, type)

    async def has_document_permission(
        self,
        document_id: int,
        type: str,
    ) -> bool:
        document_permission = await self.documentpermissions.filter(
            document_id=document_id, member_id=self.id
        ).first()
        if document_permission is None:
            document = await Document.get(id=document_id)
            return await self.has_category_permission(
                category_id=document.category_id, type=type
            )
        return getattr(document_permission, type)

    def to_dict(self) -> dict:
        return {
            "id": self.id,
            "username": self.username,
        }

    def __str__(self) -> str:
        return self.username


MemberIn = pydantic_model_creator(Member)