from tortoise import Model, fields
from tortoise.contrib.pydantic import pydantic_model_creator


class Category(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=256)

    def to_dict(self) -> dict:
        return {
            "id": self.id,
            "name": self.name,
        }

    def __str__(self) -> str:
        return self.name


class Document(Model):
    id = fields.IntField(pk=True)
    author = fields.ForeignKeyField("models.Member", on_delete="CASCADE")
    category = fields.ForeignKeyField("models.Category", on_delete="CASCADE")
    subject = fields.CharField(max_length=512)
    content = fields.TextField()

    async def to_dict(self) -> dict:
        author = await self.author.first()
        category = await self.category.first()
        return {
            "id": self.id,
            "author": author.to_dict(),
            "category": category.to_dict(),
            "subject": self.subject,
            "content": self.content,
        }

    async def to_dict_with_permissions(self) -> dict:
        author = await self.author.first()
        category = await self.category.first()
        permission = await author.documentpermissions.filter(
            document_id=self.id
        ).first()
        if permission is None:
            permission = await author.categorypermissions.filter(
                category_id=self.category_id
            ).first()
        return {
            "id": self.id,
            "author": author.to_dict(),
            "category": category.to_dict(),
            # "subject": self.subject, # to make response like the test case
            # "content": self.content, # to make response like the test case
            "can_create": permission and permission.can_create,
            "can_read": permission and permission.can_read,
            "can_update": permission and permission.can_update,
            "can_delete": permission and permission.can_delete,
        }

    def __str__(self) -> str:
        return self.subject
