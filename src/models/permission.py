from tortoise import Model, fields


class CategoryPermission(Model):
    id = fields.IntField(pk=True)
    category = fields.ForeignKeyField("models.Category", on_delete="CASCADE")
    member = fields.ForeignKeyField("models.Member", on_delete="CASCADE")
    can_create = fields.BooleanField(default=False)
    can_read = fields.BooleanField(default=False)
    can_update = fields.BooleanField(default=False)
    can_delete = fields.BooleanField(default=False)

    def __str__(self) -> str:
        return f"{self.category} - {self.meember}"


class DocumentPermission(Model):
    id = fields.IntField(pk=True)
    document = fields.ForeignKeyField("models.Document", on_delete="CASCADE")
    member = fields.ForeignKeyField("models.Member", on_delete="CASCADE")
    can_create = fields.BooleanField(default=False)
    can_read = fields.BooleanField(default=False)
    can_update = fields.BooleanField(default=False)
    can_delete = fields.BooleanField(default=False)
