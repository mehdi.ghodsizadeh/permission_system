import os
from fastapi import FastAPI
from routers import member
from tortoise.contrib.fastapi import HTTPNotFoundError, register_tortoise

from routers import document, permission, category
from db_init import init


app = FastAPI()
app.include_router(member.router, prefix="/member")
app.include_router(document.router, prefix="/document")
app.include_router(permission.router, prefix="/permission")
app.include_router(category.router, prefix="/category")

app.testing = os.getenv("TESTING", False)
init(app=app)


@app.on_event("startup")
async def startup():
    from models import Member, CategoryPermission, DocumentPermission

    await Member.get_or_create(username="admin")


@app.get("/health")
async def health():
    return {"status": "ok"}
