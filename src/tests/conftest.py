from typing import Generator
from unicodedata import category

import pytest
from fastapi.testclient import TestClient
from db_init import init
from main import app
from models.document import Category
from models.member import Member
from models.permission import CategoryPermission

# from tortoise.contrib.test import finalizer, initializer


# pytestmark = pytest.mark.anyio


@pytest.fixture(scope="session")
def client() -> Generator:
    app.testing = True

    with TestClient(app) as c:
        yield c


@pytest.fixture
async def admin():
    admin, _ = await Member.get_or_create(username="admin")
    return admin


@pytest.fixture
async def random_member():
    random_member, _ = await Member.get_or_create(username="random_member")
    return random_member


@pytest.fixture
async def random_member_with_permission():
    random_member_with_permission, _ = await Member.get_or_create(
        username="random_member_with_permission"
    )
    category, _ = await Category.get_or_create(name="Report")
    await CategoryPermission.create(
        category_id=category.id,
        member_id=random_member_with_permission.id,
        can_create=True,
        can_read=True,
        can_update=True,
        can_delete=True,
    )
    return random_member_with_permission
