from fastapi.testclient import TestClient
from models import Document


async def test_create_permission_permissions(client: TestClient, random_member):
    test_url = "/permission/create"
    cat_body = {
        "category_id": 1,
        "member_id": 4,
        "can_create": False,
        "can_read": True,
        "can_update": True,
        "can_delete": False,
    }
    # create a document for test
    test_doc = await Document.create(
        subject="test", content="test", category_id=1, author_id=1
    )

    doc_body = {
        "document_id": test_doc.id,
        "member_id": 4,
        "can_create": False,
        "can_read": True,
        "can_update": True,
        "can_delete": True,
    }

    def create_cat_requuest(username: str):
        return client.post(test_url, json=cat_body, headers={"username": username})

    def create_doc_request(username: str):
        return client.post(test_url, json=doc_body, headers={"username": username})

    res = create_cat_requuest(random_member.username)
    assert res.status_code == 403
    res = create_doc_request(random_member.username)
    assert res.status_code == 403

    res = create_cat_requuest("admin")
    assert res.status_code == 200
    cat_keys = [
        "category_id",
        "member_id",
        "can_create",
        "can_read",
        "can_update",
        "can_delete",
    ]
    cat_json = res.json()
    assert set(cat_json.keys()) == set(cat_keys)

    res = create_doc_request("admin")
    assert res.status_code == 200
    doc_keys = [
        "document_id",
        "member_id",
        "can_create",
        "can_read",
        "can_update",
        "can_delete",
    ]
    doc_json = res.json()
    assert set(doc_json.keys()) == set(doc_keys)