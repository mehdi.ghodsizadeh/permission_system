from fastapi.testclient import TestClient
from models import Member


async def test_member_lifecycle(client: TestClient):
    res = client.post("/member/create", json={"username": "test"})
    assert res.status_code == 200
    json = res.json()
    assert json["username"] == "test"
    assert json["id"] is not None
    member = await Member.filter(username="test").first()
    assert member is not None
    assert member.username == "test"
    assert member.id == json["id"]
