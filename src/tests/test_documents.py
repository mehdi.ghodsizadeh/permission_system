from fastapi.testclient import TestClient

from models import member


async def test_category_create(client: TestClient):
    URL = "/category/create"
    res = client.post(URL, json={"name": "test_category"})
    assert res.status_code == 401

    res = client.post(
        URL,
        json={"name": "test_category"},
        headers={"username": "admin"},
    )
    assert res.status_code == 200

    member_with_no_permission, status = await member.Member.get_or_create(
        username="member_with_no_permission"
    )
    res = client.post(
        URL,
        json={"name": "test_category_1"},
        headers={"username": member_with_no_permission.username},
    )
    assert res.status_code == 403


async def test_create_document(
    client: TestClient, random_member, random_member_with_permission
):
    test_url = "/document/create"
    category_id = 2
    res = client.post(
        test_url,
        json={"subject": "test_subject", "content": "test_content"},
        params={"category_id": category_id},
    )
    assert res.status_code == 401

    res = client.post(
        test_url,
        json={"subject": "test_subject", "content": "test_content"},
        params={"category_id": category_id},
        headers={"username": random_member.username},
    )
    assert res.status_code == 403

    res = client.post(
        test_url,
        json={"subject": "test_subject", "content": "test_content"},
        params={"category_id": category_id},
        headers={"username": random_member_with_permission.username},
    )
    assert res.status_code == 200
    keys = ["id", "subject", "content", "author", "category"]
    assert set(res.json().keys()) == set(keys)


def test_read_document(
    client: TestClient, random_member, random_member_with_permission
):
    document_id = 1
    test_url = f"/document/{document_id}"
    res = client.get(test_url)
    assert res.status_code == 401

    res = client.get(test_url, headers={"username": random_member.username})
    assert res.status_code == 403

    res = client.get(
        test_url, headers={"username": random_member_with_permission.username}
    )
    assert res.status_code == 200


def test_update_document(
    client: TestClient, random_member, random_member_with_permission
):
    document_id = 1
    test_url = f"/document/{document_id}"
    res = client.put(
        test_url,
        json={"subject": "test_subject", "content": "test_content"},
        headers={"username": random_member.username},
    )
    assert res.status_code == 403

    res = client.put(
        test_url,
        json={"subject": "test_subject", "content": "updated_test_content"},
        headers={"username": random_member_with_permission.username},
    )
    assert res.status_code == 200
    assert res.json()["content"] == "updated_test_content"


def test_get_all_documents(
    client: TestClient, random_member, random_member_with_permission
):
    test_url = "/document"
    res = client.get(test_url)
    assert res.status_code == 401

    res = client.get(test_url, headers={"username": random_member.username})
    assert res.status_code == 200
    assert len(res.json()) == 0

    res = client.get(
        test_url, headers={"username": random_member_with_permission.username}
    )
    assert res.status_code == 200
    assert len(res.json()) == 1
    docuement = res.json()[0]
    keys = [
        "id",
        "author",
        "category",
        "can_read",
        "can_create",
        "can_update",
        "can_delete",
    ]
    assert set(docuement.keys()) == set(keys)


def test_delete_document(
    client: TestClient, random_member, random_member_with_permission
):
    document_id = 1
    test_url = f"/document/{document_id}"
    res = client.delete(test_url, headers={"username": random_member.username})
    assert res.status_code == 403

    res = client.delete(
        test_url, headers={"username": random_member_with_permission.username}
    )
    assert res.status_code == 200
